/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package software_analise_tensao;

/**
 *
 * @author milad
 */
public class Analise {
    //VARS
    private float phi;
    private float pd;
    private float wt;
    private float np;
    private float ninterm;
    private float ng;
    private float f;
    private float vp;
    private float kv;
    private float ka;
    private float km;
    private float kl;
    private float klinterm;
    private float ks;
    private float cf;
    private float ke;
    private float v;
    private float psi;
    
    //VARS FORMULAS
    private float jbp;
    private float jbi;
    private float jbg;
    private float dp;
    private float dg;
    private float dinterm;
    private float rp;
    private float rg;
    private float rinterm;
    private float ap;
    private float cpi;
    private float cid;
    private float zpi;
    private float zig;
    private float mppi;
    private float mpig;
    private float mf;
    private float px;
    private float phin;
    private float psib;
    private float mnpi;
    private float lminig;
    private float mnig;
    private float pp;
    private float pi;
    private float pg;
    private float ipi;
    private float iig;
    private float jcp;
    private float jci;
    private float nbpinhao;
    private float nbinterm;
    private float nbengrenagem;
    private float ncpinhao_interm;
    private float ncinterm_engrenagem;
    
    
    
    /**
     * @return the phi
     */
    public float getPhi() {
        return phi;
    }

    /**
     * @param phi the phi to set
     */
    public void setPhi(float phi) {
        this.phi = phi;
    }

    /**
     * @return the pd
     */
    public float getPd() {
        return pd;
    }

    /**
     * @param pd the pd to set
     */
    public void setPd(float pd) {
        this.pd = pd;
    }

    /**
     * @return the wt
     */
    public float getWt() {
        return wt;
    }

    /**
     * @param wt the wt to set
     */
    public void setWt(float wt) {
        this.wt = wt;
    }

    /**
     * @return the np
     */
    public float getNp() {
        return np;
    }

    /**
     * @param np the np to set
     */
    public void setNp(float np) {
        this.np = np;
    }

    /**
     * @return the ninterm
     */
    public float getNinterm() {
        return ninterm;
    }

    /**
     * @param ninterm the ninterm to set
     */
    public void setNinterm(float ninterm) {
        this.ninterm = ninterm;
    }

    /**
     * @return the ng
     */
    public float getNg() {
        return ng;
    }

    /**
     * @param ng the ng to set
     */
    public void setNg(float ng) {
        this.ng = ng;
    }

    /**
     * @return the f
     */
    public float getF() {
        return f;
    }

    /**
     * @param f the f to set
     */
    public void setF(float f) {
        this.f = f;
    }

    /**
     * @return the vp
     */
    public float getVp() {
        return vp;
    }

    /**
     * @param vp the vp to set
     */
    public void setVp(float vp) {
        this.vp = vp;
    }

    /**
     * @return the kv
     */
    public float getKv() {
        return kv;
    }

    /**
     * @param kv the kv to set
     */
    public void setKv(float kv) {
        this.kv = kv;
    }

    /**
     * @return the ka
     */
    public float getKa() {
        return ka;
    }

    /**
     * @param ka the ka to set
     */
    public void setKa(float ka) {
        this.ka = ka;
    }

    /**
     * @return the km
     */
    public float getKm() {
        return km;
    }

    /**
     * @param km the km to set
     */
    public void setKm(float km) {
        this.km = km;
    }

    /**
     * @return the kl
     */
    public float getKl() {
        return kl;
    }

    /**
     * @param kl the kl to set
     */
    public void setKl(float kl) {
        this.kl = kl;
    }

    /**
     * @return the klinterm
     */
    public float getKlinterm() {
        return klinterm;
    }

    /**
     * @param klinterm the klinterm to set
     */
    public void setKlinterm(float klinterm) {
        this.klinterm = klinterm;
    }

    /**
     * @return the ks
     */
    public float getKs() {
        return ks;
    }

    /**
     * @param ks the ks to set
     */
    public void setKs(float ks) {
        this.ks = ks;
    }

    /**
     * @return the cf
     */
    public float getCf() {
        return cf;
    }

    /**
     * @param cf the cf to set
     */
    public void setCf(float cf) {
        this.cf = cf;
    }

    /**
     * @return the ke
     */
    public float getKe() {
        return ke;
    }

    /**
     * @param ke the ke to set
     */
    public void setKe(float ke) {
        this.ke = ke;
    }

    /**
     * @return the v
     */
    public float getV() {
        return v;
    }

    /**
     * @param v the v to set
     */
    public void setV(float v) {
        this.v = v;
    }

    /**
     * @return the psi
     */
    public float getPsi() {
        return psi;
    }

    /**
     * @param psi the psi to set
     */
    public void setPsi(float psi) {
        this.psi = psi;
    }
}
